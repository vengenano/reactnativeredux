import { createStore,compose,applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { combineReducer } from './../Reducers/RootCombineReducer';
const middleWare=[thunk];
const initState={};
export const centralStore =createStore(combineReducer,initState,compose(applyMiddleware(...middleWare)))


