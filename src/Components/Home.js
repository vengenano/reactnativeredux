import React, { Component } from 'react'
import { Text, View } from 'react-native'
import {connect} from 'react-redux';
import { fetchPerson } from '../Actions/PersonAction';
 class Home extends Component {
    // state = {
    //     person: [
    //         { id: 1, name: 'venge' },
    //         { id: 2, name: 'Songden' },
    //         { id: 3, name: 'Makara' }
    //     ]
    // }
    componentDidMount(){
       this.props.fetchPerson()
    }
    render() {
       const {person}=this.props
        return (
            <View>
                {person.map(objPerson => {
                    return (
                        <View key={objPerson.address}>
                            <Text>Link ={objPerson.link} Address ={objPerson.address} PhoneNumber:{objPerson.phone}</Text>  
                        </View>
                    )
                })}
            </View>
        )
    }
}
 const mapStateToProps=(state)=>{
     return{
         person:state.persons.person
     }
 }
export default connect(mapStateToProps,{fetchPerson}) (Home) 
