
// import { ActionType } from './../Actions/ActionType';
const initState={
    person:[
        { id: 1, name: 'Korean' },
        { id: 2, name: 'Cambodia' },
        { id: 3, name: 'Thailand' },
        { id: 4, name: 'Chiness' }
    ]
}
export const personReducer=(state=initState,action)=>{
    switch(action.type){
        case 'FETCH_PERSON':
        // return {...state,person:action.payload.person}
        return {...state,person:action.payload.data}
        default:
        return state
    }
    // return state
}