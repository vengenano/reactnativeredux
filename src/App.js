import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux'
import Home from './Components/Home';
import {centralStore} from './Stores/CenstralStore'
export default class App extends Component {
  render() {
    return (
      <Provider store={centralStore}>
        <Home />
      </Provider>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
